import styled from "styled-components";

export const LoginDiv = styled.div`
  background-color: #dfe0da;
  max-width: 300px;
  border-radius: 10px;
  padding: 40px 45px;
`;

export const ParentDiv = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100vh;
`;

export const LoginHead = styled.h3`
  font-size: 2rem;
  text-align: center;
  margin: 0;
  font-family: "Arial, Helvetica, sans-serif";
`;
