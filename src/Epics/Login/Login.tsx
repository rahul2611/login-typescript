import React from "react";
import Input from "../../Components/Input/Input";
import Button from "../../Components/Button/Button";
import { LoginDiv, ParentDiv, LoginHead } from "./Style";

export default function Login() {
  return (
    <ParentDiv>
      <LoginDiv>
        <LoginHead>Login</LoginHead>
        <Input placeholder="Username" />
        <Input placeholder="Password" />
        <Button name="Submit" align="center" />
      </LoginDiv>
    </ParentDiv>
  );
}
