import React from "react";
import { ButtonWrapper } from "./Style";

interface ButtonInterface {
  name: string;
  align: string;
}

export default function Button(props: ButtonInterface) {
  return (
    <ButtonWrapper align={props.align}>
      <button>{props.name}</button>
    </ButtonWrapper>
  );
}
