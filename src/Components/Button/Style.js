import styled from "styled-components";

export const ButtonWrapper = styled.div`
  margin: 10px 0;
  text-align: ${(props) => (props.align ? props.align : "auto")};
  > button {
    padding: 7px 21px;
    border: none;
    border-radius: 25px;
    background-color: #ffffff;
    font-family: "Arial, Helvetica, sans-serif";
    font-size: 1.3rem;
    text-transform: uppercase;
    font-size: 1rem;
  }
`;
