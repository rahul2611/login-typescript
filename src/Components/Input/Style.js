import styled from "styled-components";

export const InputWrap = styled.div`
  margin: 20px 0;
  > input {
    padding: 10px;
    border: none;
    border-radius: 20px;
    font-family: "Arial, Helvetica, sans-serif";
  }
`;
