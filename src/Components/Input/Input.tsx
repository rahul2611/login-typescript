import React from "react";
import { InputWrap } from "./Style";

interface InputInterface {
  placeholder: string;
}

export default function Input(props: InputInterface) {
  return (
    <InputWrap>
      <input placeholder={props.placeholder} />
    </InputWrap>
  );
}
