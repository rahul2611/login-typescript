import React from "react";
import Login from "./Epics/Login/Login";
import { ParentDiv } from "./Style";

function App() {
  return (
    <ParentDiv>
      <Login />
    </ParentDiv>
  );
}

export default App;
